const MiniCssExtractPlugin = require('mini-css-extract-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

module.exports = {
	mode: 'development',
	entry: './src/index.js',
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: 'main.js'
	},
	plugins: 
	[
		new HtmlWebpackPlugin({
			template: './src/index.html'
		}),
		new MiniCssExtractPlugin({
			filename: '[name].css'
		})
	],
	module: {
    rules: [
      {
        test: /\.scss$/i,
				use: 
				[
					MiniCssExtractPlugin.loader,
					'css-loader', // CSS' i JavaScript koduna çeviriyor.
					'sass-loader' // Sass' ı CSS' çeviriyor.
				],
      },
    ],
  },
};
